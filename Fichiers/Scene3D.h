#ifndef SCENE3D_H
#define SCENE3D_H

// taille de la fenêtre à ouvrir
#define LARG 800
#define HAUT 600
#include "Route3D.h"

extern GLuint texture1;

void InitOpenGL(void);
void DessinOpenGL(void);
void EndOpenGL(void);

void CameraPivoter(int dx, int dy);
void CameraAvancer(void);
void CameraReculer(void);
void CameraAllerGauche(void);
void CameraAllerDroite(void);
void CameraReinitialiser(void);
void toggleCameraMode(void);
void gestionCamera(void);

void VoitureDirigeableAvancer(void);
void VoitureDirigeableReculer(void);
void VoitureDirigeableAllerGauche(void);
void VoitureDirigeableAllerDroite(void);

void configurerLumieres(void);
#endif
