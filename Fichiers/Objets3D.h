// Objets3D.h : liste d'affichages prédéfinies
//

#ifndef OBJETS3D_H
#define OBJETS3D_H
#include <vector>
#include <iostream>
#include "Route3D.h"

// prototypes des fonctions de création des listes

class Vector3D
{
public :

    float x;
    float y;
    float z;

    Vector3D();

    Vector3D(float p_x, float p_y, float p_z);

    Vector3D & operator^ (const Vector3D & p_B);

    Vector3D & operator- (const Vector3D & p_B);

    Vector3D & operator+ (const Vector3D & p_B);

    Vector3D & operator* (const float lambda);

    Vector3D & normalize();

};


std::ostream& operator<< (std::ostream &strm, const Vector3D &vector);

extern GLuint newObjet(const char *nom);
extern GLuint newTube(void);
extern GLuint newPyramide4(void);
extern GLuint newCamera(void);
extern GLuint newOeil(void);
extern GLuint newSoleil(void);
extern GLuint newSphere(float r, float v, float b);
extern GLuint newVoiture(float r, float v, float b);
extern GLuint newTerrain(void);
extern GLuint newRepere3d(void);
extern GLuint newGrille(void);
extern GLuint newMaison(void);
extern GLuint newArbre(void);
extern GLuint newRoute(roadPattern* road);
extern GLuint newRoutePattern(void);
extern GLuint texture1;
extern GLuint newTunnel(void);
extern GLuint newLampadaire(void);
extern void roadAxisToFirstPoint(float patternWidth);
extern void centerRoadAxisToStraightPatternEnd(float patternLength, float patternWidth);
extern void showPatternEndAxis();
extern void showPatternStartAxis();
extern void showPatternStartBorderAxis();
extern void showPatternCenterAxis();
extern void drawRoadStraightPattern(roadPattern road, GLuint texture);
extern void drawRoadCirclePattern(roadPattern road, float stepPrecision, GLuint texture);

#endif

