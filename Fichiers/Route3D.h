#ifndef ROUTE3D_H
#define ROUTE3D_H

struct roadPattern{
    float length;
    float radius;
    const char *type;
    float width;
    float angle;
    int color;
};

#endif
