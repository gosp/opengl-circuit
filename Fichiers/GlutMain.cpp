#include <GL/gl.h>
#define GLUT_STATIC
#include <GL/glut.h>
#include <stdlib.h>

#include "Scene3D.h"

/**************************
 * Gluteries
 *
 **************************/

/** NOTE : Pour compiler, mettre les options d'édition des liens :

-lglut32
-lglu32
-lopengl32
-lwinmm
-lgdi32
-lstdc++

**/


void _DessinOpenGL()
{
    DessinOpenGL();
    glFlush();
    glutSwapBuffers();
}

enum EtatSouris {
    RIEN = 0,
    DEPL_CAM
} ModeSouris = RIEN;

int SourisRefX, SourisRefY;

void EvtSourisClic(int button, int state, int x, int y)
{
    ModeSouris = RIEN;
    if (state == GLUT_DOWN) {
        switch (button) {
            case GLUT_LEFT_BUTTON:
                ModeSouris = DEPL_CAM;
                SourisRefX = x; SourisRefY = y;
                break;
            }
        }
}

void EvtSourisMvt(int x, int y)
{
    int dx = x - SourisRefX;
    int dy = y - SourisRefY;
    switch (ModeSouris) {
        case DEPL_CAM:
            CameraPivoter(dx,dy);
            break;
    }
    SourisRefX = x; SourisRefY = y;
}

void EvtClavier(unsigned char key, int x, int y)
{
    switch (key) {
        case 27: exit(0);

            case 'z':   CameraAvancer();      return;
            case 's':   CameraReculer();      return;
            case 'q':   CameraAllerGauche();  return;
            case 'd':   CameraAllerDroite();  return;
            case '5':   CameraReinitialiser();  return;

            case 'h' : VoitureDirigeableReculer();  break;
            case 'y' : VoitureDirigeableAvancer();  break;
            case 'j' : VoitureDirigeableAllerDroite();  break;
            case 'g' : VoitureDirigeableAllerGauche();  break;

            // mode de dessin
            case 'p':   glShadeModel(GL_FLAT);      return;
            case 'l':   glShadeModel(GL_SMOOTH);    return;

            // Changement de mode de la camera
            case 'c':   toggleCameraMode();         return;
    }
}

int main (int argc, char **argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(LARG, HAUT);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("Chose3D");
	InitOpenGL();
	glutDisplayFunc(_DessinOpenGL);
	glutIdleFunc(_DessinOpenGL);
    glutMouseFunc(EvtSourisClic);
    glutMotionFunc(EvtSourisMvt);
    glutKeyboardFunc(EvtClavier);
    atexit(EndOpenGL);
	glutMainLoop();
	EndOpenGL();
	return 0;
}

