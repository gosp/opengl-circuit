#include "utils.h"
#include <stdio.h>
#include <time.h>
#include <string.h>

void log(const char* filename, const char* data) {
    FILE * pFile;
    pFile = fopen (filename,"a+");

    char result[100] = {};
        time_t now = time(0);
        char *currentTime = ctime(&now);

        strcat(result, currentTime);
        strcat(result, data);

        fputs (result, pFile);
        fclose (pFile);

}
