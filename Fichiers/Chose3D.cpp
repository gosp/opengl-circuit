// Chose3D.cpp : Définition d'une hiérarchie de classes pour les scènes 3D
//


/**************************
* Includes
**************************/

#include "StdAfx.h"
#include <GL/gl.h>
#include <GL/glu.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "Chose3D.h"
#include "Objets3D.h"
#include "iostream"
using namespace std;
// configuration du compilateur
#pragma warning(disable: 4996)  // warnings au sujet des fonctions dangereuses

// libraries à lier avec ce programme
#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")


void Chose3D::LoadIdentity(void)
{
    // initialiser la matrice de transformation
    glMatrixMode(GL_MODELVIEW);     // on se sert de ModelView temporairement
    glPushMatrix();                 // on sauve son contenu
    glLoadIdentity();               // on part de l'identité
    glGetFloatv(GL_MODELVIEW_MATRIX, transformation);   // transfo := identity
    glGetFloatv(GL_MODELVIEW_MATRIX, transfoinverse);   // inverse := identity
    glPopMatrix();                  // on remet ModelView comme elle était avant
}

float Chose3D::getX()
    {
    return (transformation[12]);
    }
    float Chose3D::getY()
    {
    return (transformation[13]);
    }
    float Chose3D::getZ()
    {
    return (transformation[14]);
    }
Chose3D::Chose3D(Chose3D *parent)
{
    // initialiser les transformations de cette chose
    LoadIdentity();

    // me rajouter dans la liste des enfants de mon parent
    if (parent != NULL) parent->enfants.push_back(this);
}


// transformations sur la matrice de transformation de cette chose
void Chose3D::Translatef(float tx, float ty, float tz)
{
    glMatrixMode(GL_MODELVIEW);     // on se sert de ModelView temporairement
    glPushMatrix();                 // on sauve son contenu

    glLoadMatrixf(transformation);  // on part de la transformation actuelle
    glTranslatef(tx,ty,tz);         // on la multiplie par cette translation
    glGetFloatv(GL_MODELVIEW_MATRIX, transformation);   // transfo := transfo * translation

    // calcul de la matrice inverse
    glLoadIdentity();               // on va faire une multiplication à droite
    glTranslatef(-tx,-ty,-tz);      // l'inverse de la translation
    glMultMatrixf(transfoinverse);  // par l'inverse de la transformation
    glGetFloatv(GL_MODELVIEW_MATRIX, transfoinverse);   // inverse := translation(-1) * inverse

    glPopMatrix();                  // on remet ModelView comme elle était avant
}

void Chose3D::Rotatef(float angle, float axex, float axey, float axez)
{
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();

    glLoadMatrixf(transformation);
    glRotatef(angle, axex,axey,axez);
    glGetFloatv(GL_MODELVIEW_MATRIX, transformation);   // transfo := transfo * rotation

    // calcul de la matrice inverse
    glLoadIdentity();
    glRotatef(-angle, axex,axey,axez);
    glMultMatrixf(transfoinverse);
    glGetFloatv(GL_MODELVIEW_MATRIX, transfoinverse);   // inverse := rotation(-1) * inverse

    glPopMatrix();  // ca remet ModelView comme elle était avant
}

void Chose3D::Scalef(float sx, float sy, float sz)
{
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();

    glLoadMatrixf(transformation);
    glScalef(sx,sy,sz);
    glGetFloatv(GL_MODELVIEW_MATRIX, transformation);   // transfo := transfo * scale

    // calcul de la matrice inverse
    glLoadIdentity();
    glScalef(1.0f/sx,1.0f/sy,1.0f/sz);
    glMultMatrixf(transfoinverse);
    glGetFloatv(GL_MODELVIEW_MATRIX, transfoinverse);   // inverse := scale(-1) * inverse

    glPopMatrix();  // ca remet ModelView comme elle était avant
}




// constructeur d'un groupe
Groupe3D::Groupe3D(Chose3D *parent) : Chose3D(parent)
{
}

// dessin des enfants d'un groupe
void Groupe3D::Dessiner(void)
{
	glPushMatrix();
	glMultMatrixf(transformation);		// transformation locale de l'objet
	// dessiner les enfants de ce groupe dans cette transformation
	for (std::list<Chose3D*>::iterator enfant = enfants.begin(); enfant != enfants.end(); enfant++)
		(*enfant)->Dessiner();
	glPopMatrix();
}



Mesh3D::Mesh3D(Chose3D *parent, const char *nom) : Chose3D(parent)
{
    // si pas de nom, revenir
    if (!nom) return;

    listeAffichage = newObjet(nom);
}

// dessin de la calllist
void Mesh3D::Dessiner(void)
{
    glPushMatrix();                     // on sauve MODELVIEW
    glMultMatrixf(transformation);      // transformation locale de l'objet
    glCallList(listeAffichage);         // dessiner cet objet
    // dessiner les enfants de cet objet dans cette transformation
    for (std::list<Chose3D*>::iterator enfant = enfants.begin(); enfant != enfants.end(); enfant++)
        (*enfant)->Dessiner();
    glPopMatrix();                      // on restitue MODELVIEW
}

Route3D::Route3D(Chose3D *parent, roadPattern* road) : Mesh3D(parent, "route")
{
    listeAffichage = newRoute(road);
}

Camera3D::Camera3D(Chose3D *parent) : Chose3D(parent)
{
    float azimut=0;
    float hauteur = 0;
}

void Camera3D::PasserDansSonRepere(void)
// modifie la matrice courante pour tout placer par rapport à la caméra
{
    // aucun pushmatrix ni popmatrix pour rendre cette transformation définitive
    glMultMatrixf(transfoinverse);
    // on a maintenant multiplié MODELVIEW par transfo inverse,
    // donc tout ce qui sera dessiné ultérieurement sera déplacé inversement
    // par rapport à la caméra
}

void Camera3D::Avancer(void)
{
    Translatef(0,0,-1);
    //cout << "camera position : (" << getX() <<", " << getY() << ", " << getZ() << endl;
}

void Camera3D::Reculer(void)
{
    Translatef(0,0,1);
}

void Camera3D::AllerGauche(void)
{

    Translatef(-1,0,0);
}

void Camera3D::AllerDroite(void)
{
    Translatef(1,0,0);
}

void Camera3D::Reinitialiser(void)
{
    //;
}

void Camera3D::Pivoter(int dx, int dy)
{
    azimut += dx/5;
    hauteur += dy/5;
    Rotatef(dx/5, 0,0.1,0);
    Rotatef(dy/5, 0.1,0,0);
}

void Camera3D::Dessiner(void)
{
    glPushMatrix();
    // ne pas transformer la caméra, ni ses enfants
    //glMultMatrixf(transformation);        // transformation locale de l'objet
    // dessiner les enfants de cet objet dans cette transformation
    for (std::list<Chose3D*>::iterator enfant = enfants.begin(); enfant != enfants.end(); enfant++) {
        //FILE*log=fopen("log.txt","a");fprintf(log,"Camera3D::Dessiner() enfant=%lx\n",*enfant);fclose(log);
        (*enfant)->Dessiner();
    }
    glPopMatrix();
}

VoitureDirigeable::VoitureDirigeable(Mesh3D *parent) : Mesh3D(parent, "voitureB"){
    vitesse = 0.2f;
    rotation = 3.0f;
    rotationY = 0.0f;
}

void VoitureDirigeable::updateRotationY(float value)
{
    rotationY = rotationY + value;
}

float VoitureDirigeable::getRotationY(void)
{
    return rotationY;
}

void VoitureDirigeable::Avancer(void)
{
    Translatef(this->vitesse,0,0);
}

void VoitureDirigeable::Reculer(void)
{
    Translatef(-vitesse,0,0);
}

void VoitureDirigeable::AllerGauche(void)
{
    Rotatef(rotation, 0, 1, 0);
    Translatef(vitesse,0,0);
    rotationY = rotationY + rotation;
}

void VoitureDirigeable::AllerDroite(void)
{
    Rotatef(-rotation, 0, 1, 0);
    Translatef(vitesse,0,0);
    rotationY = rotationY - rotation;
}
