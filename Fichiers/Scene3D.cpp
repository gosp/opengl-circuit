// Scene3D.cpp : Programme de test de Chose3D
//

/**************************
 * Includes
 **************************/

#include "StdAfx.h"
#include <GL/gl.h>
#include <GL/glu.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <vector>
#include "Chose3D.h"
#include "Scene3D.h"
#include "Objets3D.h"
#include "sdlglutils.h"

/**************************
 * Gestion de la caméra
 **************************/

Camera3D *camera;
VoitureDirigeable *voitureDirigeable;

// 1 = suit la voiture / 0 = libre
int cameraMode = 0;
int cameraReset = 0; // 1 à reset / 0 rien a faire

void CameraPivoter(int dx, int dy)
{
    camera->Pivoter(dx,dy);
}

void CameraAvancer(void)
{
    camera->Avancer();
}

void CameraReculer(void)
{
    camera->Reculer();
}

void CameraAllerGauche(void)
{
    camera->AllerGauche();
}

void CameraAllerDroite(void)
{
    camera->AllerDroite();
}

void CameraReinitialiser(void)
{
    camera->Reinitialiser();
}

void VoitureDirigeableAvancer(void)
{
    voitureDirigeable->Avancer();
}

void VoitureDirigeableReculer(void)
{
    voitureDirigeable->Reculer();
}

void VoitureDirigeableAllerGauche(void)
{
    voitureDirigeable->AllerGauche();
}

void VoitureDirigeableAllerDroite(void)
{
    voitureDirigeable->AllerDroite();
}

void toggleCameraMode(void)
{
    cameraMode = !cameraMode;
    cameraReset = 1;
}

/**************************
 * Fonction de dessin OpenGL
 **************************/

// composition de la scène 3D

Mesh3D *maison = NULL;
Mesh3D *voiture1 = NULL;
Mesh3D *voiture2 = NULL;
Mesh3D *voitureB = NULL;
Mesh3D *sol = NULL;
Mesh3D *sol2 = NULL;
Mesh3D *arbre = NULL;
Mesh3D *tube = NULL;
Mesh3D *route = NULL;
Mesh3D *tunnel = NULL;
Mesh3D *lampadaire = NULL;

// Lumières de la scène
//Lampadaire
GLenum lumiere1 = GL_LIGHT0;
GLenum lumiere11 = GL_LIGHT0;
//Phares voitures
GLenum lumiere2 = GL_LIGHT1;
GLenum lumiere3 = GL_LIGHT2;

int patternCount = 13;
roadPattern road[13];

// attributs voiture 1
int voiturePatternStep1 = 0;
float length1 = 0.0f;
float autoCarSpeed1 = 0.1f;
float angle1 = 0.0f;
float angleRestant1;
float lengthRestant1;
float restantL1;
float restantR1;
float anglePas1;

// attributs voiture 2
int voiturePatternStep2 = 0;
float length2 = 0.0f;
float autoCarSpeed2 = 0.15f;
float angle2 = 0.0f;
float angleRestant2;
float lengthRestant2;
float restantL2;
float restantR2;
float anglePas2;


void InitOpenGL(void)
{
    glShadeModel(GL_SMOOTH);

    //glEnable(GL_TEXTURE_2D);	//Active le texturing
    glEnable(GL_DEPTH_TEST);	//Active le depth test

    // mettre en place la matrice de projection
    glMatrixMode(GL_PROJECTION);
    gluPerspective(40, ((float)LARG)/HAUT, 0.1, 200);

        float roadWidth = 2.0f;
		roadPattern pattern1;
		pattern1.length = 9.0f;
		pattern1.type = "straight";
        pattern1.angle = 0.0f;
		pattern1.width = roadWidth;
		pattern1.color = 0;
		road[0] = pattern1;

		roadPattern pattern2;
        pattern2.radius = 2.0f;
		pattern2.angle = 90;
		pattern2.width = roadWidth;
		pattern2.length = (pattern2.angle * M_PI * (pattern2.radius + roadWidth / 4)) / 180.0f;
		pattern2.type = "circle";
		pattern2.color = 1;
		road[1] = pattern2;

		roadPattern pattern3;
		pattern3.length = 9.0f;
		pattern3.type = "straight";
        pattern3.angle = 0.0f;
		pattern3.width = roadWidth;
		pattern3.color = 0;
		road[2] = pattern3;

		roadPattern pattern4;
        pattern4.radius = 2.0f;
		pattern4.angle = 90;
		pattern4.width = roadWidth;
		pattern4.length = (pattern4.angle * M_PI * (pattern4.radius + roadWidth / 4)) / 180.0f;
		pattern4.type = "circle";
		pattern4.color = 1;
		road[3] = pattern4;

		roadPattern pattern5;
		pattern5.length = 9.0f;
		pattern5.type = "straight";
        pattern5.angle = 0.0f;
		pattern5.width = roadWidth;
		pattern5.color = 0;
		road[4] = pattern5;

		roadPattern pattern6;
        pattern6.radius = 2.0f;
		pattern6.angle = -90;
		pattern6.width = roadWidth;
		pattern6.length = -(pattern6.angle * M_PI * (pattern6.radius + 3*roadWidth/4)) / 180.0f;
		pattern6.type = "circle";
		pattern6.color = 1;
		road[5] = pattern6;

		roadPattern pattern7;
		pattern7.length = 9.0f;
		pattern7.type = "straight";
        pattern7.angle = 0.0f;
		pattern7.width = roadWidth;
		pattern7.color = 0;
		road[6] = pattern7;

		roadPattern pattern8;
        pattern8.radius = 2.0f;
		pattern8.angle = 90;
		pattern8.width = roadWidth;
		pattern8.length = (pattern8.angle * M_PI * (pattern8.radius + roadWidth / 4)) / 180.0f;
		pattern8.type = "circle";
		pattern8.color = 1;
		road[7] = pattern8;

		roadPattern pattern9;
		pattern9.length = 3.0f;
		pattern9.type = "straight";
        pattern9.angle = 0.0f;
		pattern9.width = roadWidth;
		pattern9.color = 0;
		road[8] = pattern9;

		roadPattern pattern10;
        pattern10.radius = 2.0f;
		pattern10.angle = 90;
		pattern10.width = roadWidth;
		pattern10.length = (pattern10.angle * M_PI * (pattern10.radius + roadWidth / 4)) / 180.0f;
		pattern10.type = "circle";
		pattern10.color = 1;
		road[9] = pattern10;

		roadPattern pattern11;
		pattern11.length = 22.0f + roadWidth;
		pattern11.type = "straight";
        pattern11.angle = 0.0f;
		pattern11.width = roadWidth;
		pattern11.color = 0;
		road[10] = pattern11;

		roadPattern pattern12;
        pattern12.radius = 2.0f;
		pattern12.angle = 90;
		pattern12.width = roadWidth;
		pattern12.length = (pattern12.angle * M_PI * (pattern12.radius + roadWidth / 4)) / 180.0f;
		pattern12.type = "circle";
		pattern12.color = 1;
		road[11] = pattern12;

		roadPattern pattern13;
		pattern13.length = 7.0f + roadWidth;
		pattern13.type = "straight";
        pattern13.angle = 0.0f;
		pattern13.width = roadWidth;
		pattern13.color = 2;
		road[12] = pattern13;


    sol = new Mesh3D(NULL, "terrain");
    sol2 = new Mesh3D(NULL, "terrain");
    tube = new Mesh3D(sol, "tube");
    maison = new Mesh3D(sol, "maison");
    arbre = new Mesh3D(sol, "arbre");
    route = new Route3D(sol, road);
	voiture1 = new Mesh3D(sol, "voitureR");
	voiture2 = new Mesh3D(sol, "voitureV");
	voitureDirigeable = new VoitureDirigeable(sol);
	tunnel = new Mesh3D(sol, "tunnel");
	lampadaire = new Mesh3D(sol, "lampadaire");

    maison->Translatef(4.5f, 0.0, -6.0f);
    sol2->Translatef(0, -0.1, -16);
	arbre->Translatef(-8.5f, 0.0, -25.0f);
    voitureDirigeable->Translatef(0.0f, 0.0f, 1.5f);
    voitureDirigeable->Rotatef(180, 0, 1, 0);
    voitureDirigeable->updateRotationY(180);

    // position initiale de la caméra : 1m50 de haut, 5m en z
    camera = new Camera3D(NULL);
    camera->Translatef(0, 80, 0);
    camera->Rotatef(-90, 1,0,0);
    voiture1->Translatef(0.0, 0.0, 0.5);
    voiture2->Translatef(0.0, 0.0, 0.5);

    lampadaire->Translatef(0.0f, 0.0f, -1.0f);
    glEnable(GL_DEPTH_TEST);
}

void DessinOpenGL(void)
{
    // effacer l'image et le z-buffer
    glClearColor (0.2f, 0.6f, 0.9f, 0.0f);      // un beau bleu ciel
    glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // initialiser la matrice d'affichage
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    // dessiner d'abord les objets liés à la caméra
    camera->Dessiner();
    camera->PasserDansSonRepere();

    gestionCamera();

    // Parcours voiture 1

    anglePas1 = road[voiturePatternStep1].angle / (road[voiturePatternStep1].length / autoCarSpeed1);
    restantL1 = road[voiturePatternStep1].length - length1;
    restantR1 = road[voiturePatternStep1].angle - angle1;

    if(restantL1 < autoCarSpeed1){
        voiture1->Translatef(restantL1, 0.0, 0.0);
        voiture1->Rotatef(restantR1, 0.0, 1.0, 0.0);

        // Change de pattern
        voiturePatternStep1++;
        voiturePatternStep1 = voiturePatternStep1 % patternCount;

        length1 = 0;
        angle1 = 0;
    }

    if(strcmp(road[voiturePatternStep1].type, "straight") == 0){
        voiture1->Translatef(autoCarSpeed1, 0.0, 0.0);
    }
    else if(strcmp(road[voiturePatternStep1].type, "circle") == 0){
        voiture1->Rotatef(anglePas1, 0.0, 1.0, 0.0);
        voiture1->Translatef(autoCarSpeed1, 0.0, 0.0);
        angle1 = angle1 + anglePas1;
    }

    length1 = length1 + autoCarSpeed1;

    // Parcours voiture 2

    anglePas2 = road[voiturePatternStep2].angle / (road[voiturePatternStep2].length / autoCarSpeed2);
    restantL2 = road[voiturePatternStep2].length - length2;
    restantR2 = road[voiturePatternStep2].angle - angle2;

    if(restantL2 < autoCarSpeed2){
        voiture2->Translatef(restantL2, 0.0, 0.0);
        voiture2->Rotatef(restantR2, 0.0, 1.0, 0.0);

        // Change de pattern
        voiturePatternStep2++;
        voiturePatternStep2 = voiturePatternStep2 % patternCount;

        length2 = 0;
        angle2 = 0;
    }

    if(strcmp(road[voiturePatternStep2].type, "straight") == 0){
        voiture2->Translatef(autoCarSpeed2, 0.0, 0.0);
    }
    else if(strcmp(road[voiturePatternStep2].type, "circle") == 0){
        voiture2->Rotatef(anglePas2, 0.0, 1.0, 0.0);
        voiture2->Translatef(autoCarSpeed2, 0.0, 0.0);
        angle2 = angle2 + anglePas2;
    }

    length2 = length2 + autoCarSpeed2;

    configurerLumieres();
    // dessiner maintenant tous les objets globaux, dans le repère relatif à la caméra
    // comme tous les objets sont parentés au sol, il sufit de dessiner le sol
    sol->Dessiner();
    sol2->Dessiner();
}

void EndOpenGL()
{
    delete voiture1;
    delete voiture2;
    delete maison;
    delete sol;
    delete arbre;
}

void gestionCamera(void){
    if(cameraMode == 1) {
        camera->LoadIdentity();
        float x = voitureDirigeable->getX();
        float y = voitureDirigeable->getY();
        float z = voitureDirigeable->getZ();
        float rotationY = voitureDirigeable->getRotationY();
        camera->Translatef(x, y, z);
        camera->Rotatef(-90, 0, 1, 0); // rotation atour de l'axe y
        camera->Rotatef(rotationY, 0, 1, 0); // rotation atour de l'axe y
        camera->Translatef(0.0f, 1.5f, +4.0f);
    }else if(cameraReset == 1){
        camera->LoadIdentity();
        camera->Translatef(0, 50, 0);
        camera->Rotatef(-90, 1,0,0);
        cameraReset = 0;
    }
}

/**
* Va configurer les différentes lumières de la scène
*/
void configurerLumieres(void){
	// Taille de la lumière 1
    // Lumière réhaussée pour donner l'impression que le lampadaire est allumé
	GLfloat lumiere1position[]={0,4.5,-1.0,1.0};
	// Direction des rayons lumineux/photos, ici en -Y soit vers le sol
	GLfloat lumiere1direction[]={0.0,-1.0,0.0};
	// La couleur de la lumière émise
	float lumiere1couleur[] = {1.0f, 1.0f, 0.0f, 1.0f}; // Blanc
	// Activation
	glEnable(lumiere1);
	// Positionnement
	glLightfv(lumiere1,GL_POSITION,lumiere1position);
	// Direction de la lumière
	glLightfv(lumiere1, GL_SPOT_DIRECTION, lumiere1direction);
	// Configuration de la lumière : ambiante, diffuse et spéculaire
	glLightfv(lumiere1, GL_DIFFUSE, lumiere1couleur);
	glLightfv(lumiere1, GL_SPECULAR, lumiere1couleur);
	glLightfv(lumiere1, GL_AMBIENT, lumiere1couleur);
	glLightfv(lumiere1, GL_EMISSION, lumiere1couleur);

	glLightf(lumiere1, GL_SPOT_CUTOFF, 50.0);

	glLightf(lumiere1, GL_SPOT_EXPONENT, 1);


    float xGauche = voitureDirigeable->getX()-2;
	float yGauche = voitureDirigeable->getY()+1;
	float zGauche = voitureDirigeable->getZ();
    float rotationY = voitureDirigeable->getRotationY();
    rotationY = (rotationY) * M_PI / -180;

	GLfloat positionPhareGauche[]={xGauche,yGauche,zGauche-3,1.0f};
	GLfloat directionPhareGauche[]={cos(rotationY),-0.5f,sin(rotationY)};
	float couleurPhareGauche[] = {1.0f, 1.0f, 0.0f, 1.0f};
	glEnable(lumiere2);
	glLightfv(lumiere2, GL_POSITION, positionPhareGauche);
	glLightfv(lumiere2, GL_SPOT_DIRECTION, directionPhareGauche);
	glLightfv(lumiere2, GL_DIFFUSE, couleurPhareGauche);
	glLightf(lumiere2, GL_SPOT_CUTOFF, 20);

    GLfloat positionPhareDroite[]={xGauche,yGauche,zGauche+3,1.0f};
	GLfloat directionPhareDroite[]={cos(rotationY),-0.5f,sin(rotationY)};
	float couleurPhareDroite[] = {1.0f, 1.0f, 0.0f, 1.0f};
	glEnable(lumiere3);
	glLightfv(lumiere3, GL_POSITION, positionPhareDroite);
	glLightfv(lumiere3, GL_SPOT_DIRECTION, directionPhareDroite);
	glLightfv(lumiere3, GL_DIFFUSE, couleurPhareDroite);
	glLightf(lumiere3, GL_SPOT_CUTOFF, 20);

    glEnable(GL_LIGHTING);

}

