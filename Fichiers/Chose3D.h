// Chose3D.h : Définition d'une hiérarchie de classes pour les scènes 3D
//

#ifndef CHOSE3D_H
#define CHOSE3D_H

/**************************
 * Includes
 **************************/

#include "StdAfx.h"
#include <GL/gl.h>
#include <GL/glu.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <list>
#include "Route3D.h"

// configuration du compilateur
#pragma warning(disable: 4996)	// fonction dangereuse

class Chose3D
{
	//// classe de base pour tous les éléments de la scène : lumières, objets et caméras

protected:	// visibles uniquement des sous-classes
	// matrice de transformation repère local->global
	GLfloat transformation[16];

	// matrice de transformation inverse (pour éviter l'invertion de matrice)
	GLfloat transfoinverse[16];

	// liste des objets enfants de celui-ci
	std::list<Chose3D*> enfants;

public:
	// initialisation
	void LoadIdentity(void);

	Chose3D(Chose3D *parent);

	// transformations sur la matrice de transformation de la chose
	void Translatef(float tx, float ty, float tz);
	void Rotatef(float angle, float axex, float axey, float axez);
	void Scalef(float sx, float sy, float sz);

    float getX();
    float getY();
    float getZ();
	// fonction de dessin, plus ou moins virtual = à réimplémenter dans les sous-classes
	virtual void Dessiner(void) = 0;
};



class Groupe3D : public Chose3D
{
public:
    Groupe3D(Chose3D *parent);

	// dessin des enfants
	void Dessiner(void);
};




class Mesh3D : public Chose3D
{
	//// description d'un mesh 3D

protected:
	// liste d'affichage du mesh
	GLuint listeAffichage;

public:
	// lecture du fichier obj ou objet prédéfini, voir plus loin
	Mesh3D(Chose3D *parent, const char *nom);

	// dessin de la calllist
	void Dessiner(void);
};

class Route3D : public Mesh3D
{
	//// description d'une route 3D depuis une source
public:
	// lecture du fichier obj ou objet prédéfini, voir plus loin
	Route3D(Chose3D *parent, roadPattern* road);
};

class Camera3D : public Chose3D
{
protected:
	/// description de la caméra
	float azimut, hauteur;

public:
	Camera3D(Chose3D *parent);

	void PasserDansSonRepere(void);
		// modifie la matrice courante pour tout placer par rapport à la caméra

	void Avancer(void);
		// dans son repère, la caméra est en 0,0,0 dirigée vers -Z

	void Reculer(void);
		// dans son repère, la caméra est en 0,0,0 dirigée vers -Z

	void AllerGauche(void);
		// dans son repère, la caméra est en 0,0,0 dirigée vers -Z

	void AllerDroite(void);
		// dans son repère, la caméra est en 0,0,0 dirigée vers -Z

	void Reinitialiser(void);

	void Pivoter(int dx, int dy);

	// dessin de la camera et des objets qui lui sont liés
	void Dessiner(void);

	void updateRotationY(float value);
};

class VoitureDirigeable : public Mesh3D
{
protected:
    float vitesse, rotation, rotationY;
public:

    VoitureDirigeable(Mesh3D *parent);

	void updateRotationY(float);

	float getRotationY(void);

	void Avancer(void);

	void Reculer(void);

	void AllerGauche(void);

	void AllerDroite(void);

};

#endif
